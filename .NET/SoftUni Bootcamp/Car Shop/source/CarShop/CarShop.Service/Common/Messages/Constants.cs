﻿namespace CarShop.Service.Common.Messages
{
    public static class Constants
    {
        public const string User = "User";
        public const string Report = "Report";
        public const string Reports = "reports";
        public const string Users = "users";
        public const string Admin = "Admin";
        public const string Pending = "Pending";
        public const string Blocked = "Blocked";
        public const int User_Id = 2;
        public const int Mechanic_Id = 3;
        public const int Blocked_Id = 4;
        public const int Pending_Id = 5;
        public const string Default_Avatar = "https://res.cloudinary.com/diihcd5cx/image/upload/v1640880258/Default_Avatar_e2kmn5.png";
    }
}