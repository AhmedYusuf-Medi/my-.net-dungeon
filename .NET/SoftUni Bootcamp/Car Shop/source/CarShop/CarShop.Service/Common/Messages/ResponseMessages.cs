﻿namespace CarShop.Service.Common.Messages
{
    public static class ResponseMessages
    {
        public const string Logout_Suceed = "Succsessfully logout";
        public const string Login_Suceed = "Successfully loged in!";
        public const string Check_Email_For_Verification = "Please check your email for verification link.";
        public const string Email_Verification_Succeed = "{0} was verified!";
        public const string Send_Mail_Succeed = "Email was successfully send!";
    }
}